from django.contrib import admin
from .models import Project
from tasks.models import Task


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ("name", "description", "owner")


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass
